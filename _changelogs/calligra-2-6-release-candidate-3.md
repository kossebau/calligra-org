---
title: Calligra 2.6 Release Candidate 3
date: 2013-01-18
author: Inge Wallin
categories: [uncategorized]
---

The Calligra team hereby announces the third release candidate of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"). The second release candidate was released without any announcement but was picked up by linux distributions and published. This release candidate fixes a very important  recalculation bug in Calligra Sheets. We urge all users to try it out so that the final release can be free of this bug.

## Try It Out

The source code of this version is available for download here: [calligra-2.5.94.tar.bz2](http://download.kde.org/unstable/calligra-2.5.94/calligra-2.5.94.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download#Unstable) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html).

Now we have an experimental package for Mac OS X. It's available for download here: [here](http://download.kde.org/unstable/calligra-2.5.93/osx/calligra-2.6.0-preview.dmg "calligra-2.6.0-preview.dmg"). More information about installation and removal can be found [here](http://www.calligra.org/get-calligra/ "Get Calligra").

The new features in version 2.6 can be seen in the announcements of the  [2.6 alpha](http://www.calligra.org/news/calligra-2-6-alpha-released/) and [2.6 beta](http://www.calligra.org/news/calligra-2-6-beta-released/) versions.

## About the Calligra Suite

Calligra is a KDE community project. Learn more at [http://www.calligra.org/](http://www.calligra.org/).
