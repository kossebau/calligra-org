---
title: Calligra Suite Goes Active
date: 2010-12-06
author: Inge Wallin
categories: [announcements,stable]
---

The KDE community today announces the start of the Calligra Suite project, a continuation of the KOffice project. The new name reflects the wider value of the KOffice technology platform beyond just desktop office applications. With a new name for the Suite and new names for the productivity applications, the Calligra community welcomes a new stage in the development of free productivity and creativity applications for desktop and mobile devices.

## Calligra Overview

The Calligra Suite contains the following applications:

**Productivity applications:**

_Words_

Word processor; new, but evolved from KWord

_Tables_

Spreadsheet program, previously known as KSpread

_Stage_

Presentation program, previously known as KPresenter

_Flow_

Flowchart program, previously known as Kivio (will be released in the next version)

_Kexi_

Database application like Microsoft Access

**Management applications:**

_Plan_

Project management, previously known as KPlato

**Graphics applications:**

_Krita_

Drawing application for multi-layered pixel-based images

_Karbon_

Drawing application for multi-layered vector images

More applications will be added as we get more contributors. Currently all applications except Calligra Words will be maintained by their respective KOffice developers.

The Calligra Suite introduces the Calligra Office Engine which makes it easy for developers to create new user experiences, target new platforms and create specialized versions for new kinds of users. Currently, there are two main user experiences: the desktop UI with the applications mentioned above, and FreOffice which is the only free mobile office suite in existence.

## Calligra and Users

It is a goal of the Project that users will see only positive changes. In addition to new names, we expect that the move to Git will make development speedier and higher quality. New user experiences will be added and the existing ones will see more usability work.

The community will release KOffice version 2.3 as planned in approximately 2 weeks, and will maintain it with bugfix releases during the KOffice 2.3 life cycle. Starting with version 2.4, we will continue development of most of the applications under the new names.

## Calligra and the Past

Nearly everyone in the KOffice community has joined together to make this move. Leaving the past behind us, we are excited at this opportunity to make our software more innovative and widely-used. At the same time that the Calligra project is created, we will move from Subversion to Git, making it an even better platform for innovation in the free office space.

## Calligra and KDE

KDE is one of the largest open source communities in the world. KDE software is one of only two projects that has reached [a million commits](http://blogs.fsfe.org/padams/?p=140) in their repositories (the other is Apache).

KDE publishes several workspaces (Plasma Desktop, Plasma Netbook and soon Plasma Mobile), a development platform, and many sets of applications like the KDE EDU applications, KDE games, Amarok and K3b.

KOffice has always been part of the larger KDE community. Calligra will continue to use the KDE infrastructure such as bugtracker, forums, and community wikis. The Calligra team is very much a part of the KDE community and proud of it. The applications we develop will continue to be KDE offerings.

## Resources

Find out more about Calligra Suite here:

For users: User Web Site: [http://www.calligra-suite.org/](../ "http://www.calligra-suite.org/") (this site) User Forum: [http://forum.kde.org/viewforum.php?f=203](http://forum.kde.org/viewforum.php?f=203 "http://forum.kde.org/viewforum.php?f=203") Facebook Page: [http://www.facebook.com/pages/Calligra/161929600510563](http://www.facebook.com/pages/Calligra/161929600510563 "http://www.facebook.com/pages/Calligra/161929600510563") Facebook Group: [http://www.facebook.com/home.php?sk=group\_176774135672923](http://www.facebook.com/home.php?sk=group_176774135672923 "http://www.facebook.com/home.php?sk=group_176774135672923")

For developers: Developer Mailing List: [https://mail.kde.org/mailman/listinfo/calligra-devel](https://mail.kde.org/mailman/listinfo/calligra-devel "https://mail.kde.org/mailman/listinfo/calligra-devel") Developer Wiki: [http://community.kde.org/Calligra](http://community.kde.org/Calligra "http://community.kde.org/Calligra") Source code: [https://projects.kde.org/projects/calligra/repository](https://projects.kde.org/projects/calligra/repository "https://projects.kde.org/projects/calligra/repository")

Existing web sites for individual applications like [krita.org](http://www.krita.org/ "Krita website") and [kexi-project.org](http://kexi-project.org/ "Kexi website") will continue to work as before.
