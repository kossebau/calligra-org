---
title: Calligra 2.9.2 Released
date: 2015-04-02
author: jstaniek
categories: []
---

The Calligra team has released version 2.9.2, a the bugfix release of the [Calligra Suite](http://www.calligra.org "Home page of the Calligra Suite"), Calligra Active and the Calligra Office Engine. Updating the software is recommended to everybody.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-2-released/#support)

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that may be not mentioned here.

### General

- Re-enable Review and References features that were disabled by mistake
- Fix crash after showing "Insert Bibliography" second time (bug 335458)
- Fix bug "Can't change footnotes style" (bug 303489)
- Do not crash when reducing number of columns on page (bug 322429)
- Do not crash after selecting more than one table and pressing Ctrl+C (bug 330284)

### Kexi

- _General_:
    - Fix error when configuring Calligra (related to the Marble maps component) (bug 345318)
- _Forms & Table views_:
    - Make combo-boxes in table and form views show just one column (bug 345631)
    - Fix displaying records in data aware combo boxes (bug 345350)
- _Forms_:
    - Fix crash when removing elements in the object tree (bug 345336)

### Krita

- Don't show the zoom level on-canvas message while loadidng
- Fix initialization of the multi-brush axis
- Add some more kickstarter backers to the about box
- Fix memory leak when loading presets (and a bunch more memory leaks)
- Fix crashes related to progress reporting when running a g'mic action
- Add a toggle button to hide/show the filter selection tree in the filter dialog
- Fix a focus bug that made it hard to edit e.g. layer names when activating the editor in the docker with a tablet stylus
- Fix geometry of the toolbox on startup in some cases
- Fix lock proportions in the free transform tool when locking aspect ratio
- Add an option to hide the docker titlebars
- Update the resource manager lists after loading a resource bundle
- Make the resource manager look for bundles by default
- Make the eraser end of the stylus erase by default
- Make krita remember the presets chosen for each stylus and stylus end
- Make Krita start faster by only loading images for the references docker when the references docker is visible
- Fix a crash in the g'mic docker when there's no preview widget selected
- On switching images, show the selected layer in the layer box, not the bottom one
- Show the selected monitor profile in the color management settings page instead of the default one
- Make the Image Split dialog select the right export file type.
- Fix saving and loading masks for file layers
- Make the default MDI background darker
- Fix loading some older .kra files that contained an image name with a number after a /
- Don't crash if the linux colord colormanager cannot find a color-managed output device
- Clean the code following a number of PVS studio code analyzer warnings
- Add tooltips to the presets in the popup palette
- Fix a problem where brush presets in the popup palette were sometimes misaligned

### Calligra Words

- Fix a crash on Format page (bug 345273)
- Fix memory leaks
- Make line number of cursor (statusbar) count lines of text instead of hard breaks (bug 298365)
- Many fixes for sections

  

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.9.2.tar.xz](http://download.kde.org/stable/calligra-2.9.2/calligra-2.9.2.tar.xz). Also [translations to many languages](http://download.kde.org/stable/calligra-2.9.2/calligra-l10n/) and [MD5 sums](http://download.kde.org/stable/calligra-2.9.2/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release).

  

## What's Next and How to Help?

The next step after the [2.9 series](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) is Calligra 3.0 which will be based on new technologies. We expect it later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated to [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita). Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, traveling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to: ![Heart](/assets/img/heart-701.png)

**Support entire Calligra indirectly by donating to KDE**, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations).

![Heart](/assets/img/heart-701.png)

**Support Krita directly by donating to the Krita Foundation**, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/).

![Heart](/assets/img/heart-701.png)

**Support Kexi directly by donating to its current BountySource fundraiser**, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

Calligra Suite is a graphic art and office suite developed by the KDE community. It is available for desktop PCs, tablet computers and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics and digital painting. For more information visit [calligra.org](http://www.calligra.org/).

  

## About KDE

![](/assets/img/klogo-official-lineart_simple_bw-128x128.png)

[KDE is an international technology team](https://www.kde.org/community/whatiskde/) that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet, multimedia, entertainment, education, graphics and software development. KDE's software available in more than 60 languages on Linux, BSD, Solaris, Windows and Mac OS X.

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
