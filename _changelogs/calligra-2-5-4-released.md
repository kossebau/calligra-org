---
title: Calligra 2.5.4 Released
date: 2012-11-21
author: Inge Wallin
categories: []
---

The Calligra team has released version 2.5.4, the fourth bugfix release of the [Calligra Suite,  and Calligra Active](http://www.calligra.org/). This release contains a number of important bug fixes to 2.5.3 and we recommend everybody to update.

## Bugfixes in This Release

Here is an overview of the most important fixes that are in 2.5.4. There are several others that are not mentioned here.

### General:

- Fix configuration of FreeTDS for fresh builds
- Fix: Autosave only triggered on first change
- Disabled krita colorspace plugins for braindump, flow, karbon, avoid possible crash
- Fix compilation on ARM

### Stage:

- Fix tooltips/whatsthis for slide navigation actions
- Fix bad offset of cursor markers in rulers if document is smaller than canvas (Stage/Flow)

### Kexi:

- Kexi Forms: Fix for Text Editor stripping out rich text formatting (bug 309174)
- Kexi Forms: Fix "data source tag" for text box
- Kexi Forms: Make palette background color property in text editor work (bug 309274)
- Kexi Table Designer: Remove limit of 101 fields (bug 309116)

Filters:

- HTML export: Don't write empty css stylesheet

## Try It Out

The source code of this version is available for download here: [calligra-2.5.4.tar.bz2](http://download.kde.org/stable/calligra-2.5.4/calligra-2.5.4.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html). There is also a separate package or [Krita](http://www.krita.org/) only.

We would welcome volunteers who want to build and test the Calligra Suite on OSX.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
