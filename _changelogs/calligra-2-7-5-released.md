---
title: Calligra 2.7.5 Released
date: 2013-11-27
author: cyrilleberger
categories: []
---

The Calligra team has released version 2.7.5, the fourth of the bugfix releases of the [Calligra Suite, and Calligra Active](http://www.calligra.org/) in the 2.7 series. This release contains a few important bug fixes to 2.7.4 and we recommend everybody to update.

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that are not mentioned here.

### Common

- Fix bug 326158: the text color in the modebox tab icons were not drawn accoring to style.
- Make spellchinking work with more dictionaries.
- Fix bug 290999: make sure we erase an old selection in another textshape whenever we make a new selection.
- Fix crash 326243 and 325547.
- Fix a crash while saving embedded documents like charts.

### Kexi

- Fix bug 327995: Make changes to query design reflected in reports after saving
- Fix bug 327993: Make records in reports sorted according to the sorting of the query
- If --driver is missing but connection options specified, display a note and exit
- Fix bug 319432: Fixed crash when using command line "kexi --dbdriver postgresql"
- Fix regression in password dialog.

### Krita

- Fix untranslatable strings.
- Fix crash on malformed pdf.
- Fix fullscreen in canvas-only mode in Krita.
- Fix bug 321100: do not write the implicitely written "mimetype" entry a 2nd time on saving kra files (and thus avoid assumed bug in KZip)

### Plan

- Set First Day of Week to Monday if using ISO Week for week numbering
- Fix crash in Plan on loading work packages into gantt view.

### Sheets

- Update mimetypes for the CSV export to make it work.
- Fix bug 298155: Fix a crash when using array values and make array values render correctly when used
- Fix bug 178172 and 181576: fix text to columns.
- Don't fail loading a file when the comments fail to load.

### Try It Out

- **The source code** is available for download: [calligra-2.7.5.tar.xz](http://download.kde.org/stable/calligra-2.7.5/calligra-2.7.5.tar.xz).

As far as we are aware, the following distributions package Calligra 2.7. This information will be updated when we get more details. In addition, many distributions will package and ship Calligra 2.7 as part of their standard set of applications.

- In **Chakra Linux**, Calligra is the default office suite so you don't have to do anything at all to try out Calligra. Chakra aims to be a [showcase Linux for the "Elegance of the Plasma Desktop"](http://www.chakra-project.org/) and other KDE software.
- Users of **Ubuntu and Kubuntu** are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:
    
    sudo add-apt-repository ppa:neon/ppa \\
    && sudo apt-get update\\
    && sudo apt-get install project-neon-base \\
       project-neon-calligra \\
       project-neon-calligra-dbg
    
    You can run these packages by adding /opt/project-neon/bin to your PATH.
- **Arch Linux** provides Calligra packages in the \[kde-unstable\] repository.
- **Fedora** packages are available in the rawhide development repository ([http://fedoraproject.org/wiki/Rawhide](http://fedoraproject.org/wiki/Rawhide)), and unofficial builds are available for prior releases from kde-unstable repository at [http://kde-redhat.sourceforge.net/](http://kde-redhat.sourceforge.net/) .
- **OpenSUSE** Calligra packages for openSUSE are available in repository [http://download.opensuse.org/repositories/KDE:/Extra/](http://download.opensuse.org/repositories/KDE:/Extra/).
- Calligra **FreeBSD** ports are available in [Area51](http://freebsd.kde.org/area51.php).
- **MS Windows** installer will be available from [KO GmbH](http://www.kogmbh.com/download.html).
- **Mac OS X:** We would welcome volunteers who want to build and publish packages for the Calligra Suite on OS X.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
