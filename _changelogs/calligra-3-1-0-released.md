---
title: Calligra 3.1.0 released
date: 2018-02-01
author: danders
categories: []
---

We are pleased to announce the release of Calligra 3.1.0 with the following apps included: Words, Sheets, Karbon, Gemini, and Plan.

Note that Gemini, the KDE Office suite for 2-in-1 devices, is back after missing from the initial Calligra 3.0 release.

Also note that Kexi, the visual database applications creator is [close to release 3.1.0](https://community.kde.org/Kexi/Releases#3.1.0_Beta_1).

The following is a list of new features and bug fixes since the last release (3.0.1).

**Common:**

- Picture shape tool: Paint crop rectangle and its handles with 1px wide outline. (BUG: 388930) Due to the painter scaling the pen width (1 by default) was scaled too, which caused the outlines to cover the whole image.
- Textlayout: Do not enter infinite loop when line rect is not valid.
- Add RTF support to Okular. (BUG: 339835)

**Sheets:**

- LaTeX export: Fix typo in UI string. (BUG: 380030)

**Plan:**

-  Add dialog to be able to edit multiple tasks at once. (BUG: 310937)
-  Provide expand all/collapse all in context menu. (BUG: 313606) Expands/collapses selected item(s) and all children. Retains the treeviews expanded rows across operations.
-  Printing: Make changes to page layout persistent. (BUG: 385084)
-  Open Document Text format report generator added. Adds the abillity to generate reports in odt format directly. Reports can be viewed using an odt viewer like Calligra Words or LibreOffice Writer. Report templates are also in odt format and can be designed using e.g Words or Writer.
- Add support for sharing resources in multiple projects.
- Improved context help and documentation.
- Add support for automatic holidays generation.
- Calendar view: Handle context menus with no calendar.
- Replace the file centric startup page with page more suitable for projects.
- Add editing and reloading of task modules.
- Gantt view: Fix possible crash when deleting gantt view with large projects.
- Gantt view: Fix issue with task dependencies not cleared in certain situations.
- Fix undo/redo issue with modifying project target times.
- Fix potential crash in Cost Breakdown View.
- Fix potential crash when creating new project from current project.
- Fix performance issue in chart view.
- Note: Support for scripting is discontinued and reports using KReport is not supported in this release.

**Gemini:**

- Port to KDE Frameworks 5
- Port the welcome screen to Kirigami
- Port to using the Qt Quick 2 based Calligra components (based on work done for Jolla Documents)
- Port to using libgit2 directly for git support (as the Qt support library has become unmaintained)
- Fix template support
