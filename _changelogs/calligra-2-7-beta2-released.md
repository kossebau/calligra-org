---
title: Calligra 2.7 Beta2 Released
date: 2013-05-28
author: Inge Wallin
categories: []
---

The Calligra team announces the second beta release of version 2.7 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). This release is a bugfix release in preparation for the final release of Calligra 2.7 in June.

## Updates in This Release

Calligra 2.7 beta2 has a number of important fixes from the first beta. Here is an excerpt of the most important ones.

**Words,** the word processing application, has fixes in the bibliography editor and the WordPerfect import filter is now enabled again.

**Kexi**, the visual database creator, has fixes in the database engine, fixes in the password dialog for protected databases.

**Krita,** the 2D paint application, has fixes for the freehand strokes, some brush presets, a modifier bug (#317200), a selection bug (#316715), a fix for drag&drop into the layer box, a fix for creating a new image from a the clipboard (#315778), correctly scaling of "marching ants" (#300482), high CPU load when changing brush sizes (#315374), a drag&drop crash with layers (#318713), some other fixes for drag&drop of images (among them #318560, #319330, #319332, #319334).

## Try It Out

The source code of this release is available for download: [calligra-2.6.91.tar.bz2](http://download.kde.org/unstable/calligra-2.6.91/calligra-2.6.91.tar.bz2). Alternatively, you can [download binaries for many Linux distributions and windows](http://userbase.kde.org/Calligra/Download#Unstable_Release).

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
