---
title: Calligra 2.9.5 Released
date: 2015-06-09
author: jstaniek
categories: []
---

We are pleased to announce that [Calligra Suite, and Calligra Active 2.9.5](http://www.calligra.org) have just been released. This recommended update brings further improvements to the 2.9 series of the applications and underlying development frameworks.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-5-released/#support)

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that may be not mentioned here.

### General

- Avoid crash when multiple templates are found.
- Fix lookup of templates.
- Improve usability of the Find toolbar used in Words.

### Kexi

- _General_:
    - Do not show two Project windows after double-clicking on the Welcome page's item. (bug 346902)
- _Queries_:
    - Use ILIKE/NOT ILIKE operator instead of LIKE for PostgreSQL. This is good for consistency with other database backends since Kexi's default LIKE is case-insensitive. (bug 348203)
    - Fix crash for queries where lookup table is aliased. (bug 330410)
- _Reports_:
    - Multiple usability improvements for data source combo box. Fixed sorting, field selection, unnecessary empty item. (bugs 346449, 347127)
    - Maps element: allow to set map properties using scripts; 2 or 3 fields of data can be passed to an item.

### Krita

- _New Features_:
    - Add a lightness curve to the per-channel filter (bug 324332)
    - Add a brush preset history docker (bug 322425)
    - Add an all-files option to the file-open dialog
    - Add global light to the layer styles functionality (bug 348178)
    - Allow the user to choose a profile for untagged PNG images (bug 345913, 348014)
    - Add a built-in performance logger
    - Added a default set of paintop preset tags (these are not deletable yet!)
    - Add support for author profiles (default, anonymous, custom) to .kra files
    - Add buttons and actions for layer styles to the Layer docker
    - Add ctrl-f shortcut for re-applying the previously used filter (bug 348119)
    - Warn Intel users that they might have to update their display driver
    - Implement loading/saving of layer styles to PSD files
    - Add support for loading/saving patterns used in layer styles
    - Allow inherit alpha on all types of layers
    - Add a pass-through switch for group layers (bug 347746, 185448)
    - Implement saving of group layers to PSD
    - Add support for WebP (on Linux)
    - Add a shortcut (Ctrl-Shift-N) for edit/paste into New Image (bug 344750)
    - Add on-canvas preview of the current color when picking colors (bug 338128)
    - Add a mypaint-style circle brush outline.
    - Split the cursor configuration into outline selection and cursor selection
    - Add loading and saving transparancy masks to PSD groups
- _Performance improvements_:
    - Remove delay on stroke start when using Krita with a translation
- _Bug fixes_:
    - Fix view rotation menu
    - Fix crash when duplicating a global selection mask (bug 348461)
    - Improve the gui for the advanced color selector settings
    - Fix resetting the number of favorite presets in the popup (bug 344610)
    - Set proper activation flags for the Clear action (bug 34838)
    - Fix several bugs handling multiple documents, views and windows (bug 348341, bug 348162)
    - Fix the limits for document resolution (bug 348339)
    - Fix saving multiple layers with layer styles to .kra files (bug 348178)
    - Fix display of 16 bit/channel RGB images (bug 343765)
    - Fix the P\_Graphite\_Pencil\_grain.gih brush tip file
    - Fix updating the projection when undoing removing a layer (bug 345600)
    - Improve handling of command-line arguments
    - Fix the autosave recovery dialog on Windows
    - Fix creating templates from the current image (bug 348021)
    - Fix layer styles and inherit alpha (bug 347120)
    - Work around crash in the Oxygen widget style when animations are enabled (bug 347367)
    - When loading JPEG files saved by Photoshop, also check the metadata for resolution information (bug 347572)
    - Don't crash when trying to isolate a transform mask (transform masks cannot be painted on) (bug 347622)
    - Correctly load Burn, Color Burn blending modes from PSD (bug 333454)
    - Allow select-opaque on group layers (bug 347500)
    - Fix clone brush to show the outline even if it's globally hidden (bug 288194)
    - Fix saving of gradients to layer styles
    - Improve the layout of the sliders in the toolbar
    - Fix loading floating point TIFF files (bug 344334)
  

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.9.5.tar.xz](http://download.kde.org/stable/calligra-2.9.5/calligra-2.9.5.tar.xz). Also [translations to many languages](http://download.kde.org/stable/calligra-2.9.5/calligra-l10n/) and [MD5 sums](http://download.kde.org/stable/calligra-2.9.5/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release).

  

## What's Next and How to Help?

The next step after the [2.9 series](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) is Calligra 3.0 which will be based on new technologies. We expect it later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated to [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita). Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, travelling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to: ![Heart](/assets/img/heart-701.png)

**Support entire Calligra indirectly by donating to KDE**, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations).

![Heart](/assets/img/heart-701.png)

**Support Krita directly by donating to the Krita Foundation**, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/).

![Heart](/assets/img/heart-701.png)

**Support Kexi directly by donating to its current BountySource fundraiser**, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

Calligra Suite is a graphic art and office suite developed by the KDE community. It is available for desktop PCs, tablet computers and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics and digital painting. For more information visit [calligra.org](http://www.calligra.org/).

  

## About KDE

![](/assets/img/klogo-official-lineart_simple_bw-128x128.png)

[KDE is an international technology team](https://www.kde.org/community/whatiskde/) that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet, multimedia, entertainment, education, graphics and software development. KDE's software available in more than 60 languages on Linux, BSD, Solaris, Windows and Mac OS X.

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
