---
title: Calligra 2.4 Released
date: 2012-04-11
author: Inge Wallin
categories: []
---

The Calligra team is very proud to announce the first release ever of the [Calligra Project](http://www.calligra.org/ "Home page of the Calligra Project"), version 2.4.0. This is the end of a very long journey that started over a year ago and has now lead up to the first stable release of Calligra.

\[caption id="attachment\_3225" align="aligncenter" width="500" caption="Krita 2.4 with a piece of art by David Revoy"\][![](/assets/img/krita-screen_01_davidrevoy-500x301.jpg "krita-screen_01_davidrevoy")](http://www.calligra.org/news/calligra-2-4-released/attachment/krita-screen_01_davidrevoy/)\[/caption\]

## Introduction to Calligra

Calligra is an integrated suite of applications that cover office, creative and management needs. It's the most comprehensive free suite of applications anywhere and we expect the number of applications to grow as the project matures. Calligra offers its applications on both desktop computers and mobile platforms like tablets and smartphones.

The desktop version of Calligra is called the Calligra Suite. Version 2.4 of the Calligra Suite contains the following applications:

### Office applications

- **Calligra Words** Word processor
- ****Calligra** Sheets** Spreadsheet
- ****Calligra** Stage** Presentation
- ****Calligra** Flow** Diagrams and Flowcharts
- **Kexi** Visual Database Creator
- **Braindump** Note Taking

### Graphic applications

- **Krita** Drawing Application
- **Karbon** Vector Graphics

### Management applications

- ****Calligra** Plan** Project Management

There are also versions for mobile platforms and these have their own user interfaces suited for touch computing. This release contains two such user interfaces:

- **Calligra Mobile** A version of Calligra for the Nokia N900 smartphone . This version is both a viewer and a document editor. It supports text documents, spreadsheets and presentations.
- **Calligra Active** A more modern user interface for Calligra that is well integrated into the [Plasma Active](http://plasma-active.org/ "Plasma Active") tablet environment. At this time Calligra Active is a viewer only.

Several companies have used Calligra as a base for their own office solution. Nokia uses the Calligra Office engine in the Documents application on Harmattan Meego. Every N9 user already has Calligra installed on their phone!

Calligra uses the [Open Document Format](http://en.wikipedia.org/wiki/Odf) (ODF) as its main file format which makes it compatible with most other office applications including OpenOffice.org, LibreOffice and Microsoft Office. It can also import the native file formats of Microsoft Office with great accuracy, in many cases the best you can find.

The applications in the Calligra Suite shares some common UI concepts that gives it a modern look better suited for the wide screens of today. One of them is that most formatting is done using dockers which are placed at the side of the windows instead of on the top. This makes more space available for the actual document contents and avoids opening dialogs on top of it. If the user chooses, s/he can rearrange the placement of the dockers around the document area or even tear loose them and let them float freely. The arrangement is saved and reused the next time Calligra is opened.

\[caption id="attachment\_3229" align="aligncenter" width="500" caption="The user interface of Calligra Words"\][![](/assets/img/calligrawords-whitepaper2-500x338.png "calligrawords-whitepaper2")](http://www.calligra.org/news/calligra-2-4-released/attachment/calligrawords-whitepaper2/)\[/caption\]

## News in This Release

This is the first release of Calligra so maybe talking about news is not the thing to do. However, Calligra is a continuation of the old KOffice project and it may be interesting for KOffice users to know what they will get. Here are some highlights:

- Calligra now has a **completely rewritten text layout engine** that can handle most of the advanced layout features of ODF. This includes tables that can now span more than one page, footnotes and endnotes and correct run-around around other objects such as pictures. This text layout engine is used all over the suite. The Words application itself is also largely rewritten but this is not as visible to the user.
- The **user interface is simplified**. In Calligra Words multiple docks are now combined into one area controlling most aspects of text formatting. Kexi introduced modern modeless views.
- Calligra can now handle **larger parts of the ODF specification**. One example of this is handling line endings like arrows.
- There are **several new applications** including Flow, the diagram application. Braindump, the note taking applications is another new member of the Calligra family.
- Calligra Active is a **new interface for touch based devices** and especially for the Plasma Active environment.
- **Import filters** for Microsoft document formats have been enhanced to a new level.
- On the technical side handling of styles, especially style inheritance works now. Roundtripping of advanced documents has also been improved a lot.
- And of course many many bugs have been fixed as well as enhancements to almost all small or big parts of Calligra (see also the [Kexi changelog](http://community.kde.org/Kexi/Releases/Kexi_2.4#List_of_changes)).

## Try It Out

The source code of this version is available for download here: [calligra-2.4.0.tar.bz2](http://download.kde.org/stable/calligra-2.4.0/calligra-2.4.0.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download#Stable) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html).

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Project

Calligra is a KDE community project. Learn more at [http://www.calligra.org/](http://www.calligra.org/) and [http://www.kde.org/](http://www.kde.org/) .
