---
title: Calligra 2.6.4 Released
date: 2013-06-04
author: Inge Wallin
categories: []
---

The Calligra team has released version 2.6.4, probably the last of the bugfix releases of the [Calligra Suite, and Calligra Active](http://www.calligra.org/) in the 2.6 series. This release contains a number of important bug fixes to 2.6.3 and we recommend everybody to update.

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that are not mentioned here.

### Words:

- Fix the mimetype for the word perfect import filter. This makes the word perfect import work again.
- Fix a problem with repeated header rows in tables.
- Fix a crash in the document structure docker (#317975).

### Author:

- Author got a new application icon.

### Kexi:

- A fix in the database engine.
- Fix in the query save dialog.
- Fix saving new query designs in the SQL view.
-  Fix the look of the modern menu and tabbed toolbar for the bespin widget style.

### Try It Out

- **The source code** is available for download: [calligra-2.6.4.tar.bz2](http://download.kde.org/stable/calligra-2.6.4/calligra-2.6.4.tar.bz2). As far as we are aware, the following distributions package Calligra 2.6. This information will be updated when we get more details. In addition, many distributions will package and ship Calligra 2.6 as part of their standard set of applications.
- In **Chakra Linux**, Calligra is the default office suite so you don't have to do anything at all to try out Calligra. Chakra aims to be a [showcase Linux for the "Elegance of the Plasma Desktop"](http://www.chakra-project.org/) and other KDE software.
- Users of **Ubuntu and Kubuntu** are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:
    
    sudo add-apt-repository ppa:neon/ppa \\
    && sudo apt-get update\\
    && sudo apt-get install project-neon-base \\
       project-neon-calligra \\
       project-neon-calligra-dbg
    
    You can run these packages by adding /opt/project-neon/bin to your PATH.
- **Arch Linux** provides Calligra packages in the \[kde-unstable\] repository.
- **Fedora** packages are available in the rawhide development repository ([http://fedoraproject.org/wiki/Rawhide](http://fedoraproject.org/wiki/Rawhide)), and unofficial builds are available for prior releases from kde-unstable repository at [http://kde-redhat.sourceforge.net/](http://kde-redhat.sourceforge.net/) .
- **OpenSUSE** Calligra packages for openSUSE 12.3 are available in repository [http://download.opensuse.org/repositories/KDE:/UpdatedApps/openSUSE\_12.3/](http://download.opensuse.org/repositories/KDE:/UpdatedApps/openSUSE_12.3/). The latest [KDE SC stable release](http://download.opensuse.org/repositories/KDE:/Release:/410/openSUSE_12.3/) also includes the new stable Calligra.
- Calligra **FreeBSD** ports are available in [Area51](http://freebsd.kde.org/area51.php).
- **MS Windows** installer will be available from [KO GmbH](http://www.kogmbh.com/download.html).
- **Mac OS X:** We would welcome volunteers who want to build and publish packages for the Calligra Suite on OS X.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
