---
title: Calligra 2.4 Beta 7
date: 2012-02-01
author: Inge Wallin
categories: [announcements]
---

The Calligra team is proud to announce the seventh beta version of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"). This is hopefully the last beta before the first release candidate but there may still be more if any new serious issues are found.

News in This Release

There is now a hard feature freeze and no more new features are allowed into Calligra. Therefore almost all changes since the last beta version are bugfixes. One new feature that did sneak in despite the freeze is an **improved text statistics docker** that lets the writer view the document statistics (#words, #characters, etc) in real time.

The focus of this release has been to continue on **improvements of saving and roundtripping**. This means that a document should contain the same information after loading and saving it as it did before the cycle. This is the final focus area that needs to be addressed before the final release.

Other than saving, one bad set of **issues with inheritance of styles** was fixed in this release and of course the usual amount of crashes and normal bugs are fixed. And also as usual the team is grateful for all bug reports in this and other areas.

The mobile user interface **Calligra Active** is not affected by the freeze and has two new features: **Selection and Copy for text** and a **Custom Slideshow** feature. Calligra Active was also featured in an [article on Linux Weekly News](http://lwn.net/Articles/476955/ "LWN: Porting office suites to mobile platforms") about mobile versions of free office suites. It's well worth a read. (For people reading this on February 1st or earlier the article is for subscribers only but it will be made available for the public on February 2nd.)

## Try It Out

The source code of this version is available for download here: [calligra-2.3.87.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.87/calligra-2.3.87.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download#Unstable) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html).

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
