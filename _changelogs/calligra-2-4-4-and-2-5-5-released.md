---
title: Very Important Update for Calligra 2.4 and 2.5 Released
date: 2013-01-23
author: jstaniek
categories: []
---

**Attention, this is a crucial update!**

The Calligra team releases updates to the 2.4 and 2.5 series at the same time: versions  2.4.4 and 2.5.5. These releases include fixes for two major bugs in the [Sheets application](http://www.calligra.org/sheets/) which made recalculations of spreadsheet contents go wrong under certain conditions. We urge all users of Calligra Sheets to update immediately.

In addition these releases contain a number of other important bug fixes.

## Bugfixes in This Release

Here is an overview of the most important fixes that are in 2.4.4 and 2.5.5. There are several others that are not mentioned here.

### General:

- 2.5.5: A few build issues on Windows.

### Sheets:

- 2.4.4 and 2.5.5: A very nasty bug which made recalculations of spreadsheet contents go wrong under some circumstances (BUGS: 312981, 313010)

### Kexi:

- Kexi Forms:
    - 2.5.5: Description was ignored in Command Link button
    - 2.4.4: Make palette background color property in text editor work (BUG: 309274)
    - 2.4.4: Fix "data source tag" for text box (regression because of changes in KDE libraries' line editor)
- 2.4.4: SQLite Driver: Fix possible data loss of compacted file (when process tools crash or fail for any reason)
- 2.4.4: Remove limit of 101 fields in Kexi Table Designer (BUG: 309116)
- 2.4.4 and 2.5.5: Fix MySql login failure when not saving password - fix for case 3.1 (BUG: 313025)
- 2.4.4 and 2.5.5: Fix possible crash when pressing tab in the global search box
- 2.5.5: Property editor: make items are always sorted by key

## Try It Out

The source code of this version is available for download from [download.kde.org](http://download.kde.org/stable/calligra-2.5.5/calligra-2.5.5.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download) are available for various operating systems and distributions. This release will be packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can soon find it on their [download page](http://www.kogmbh.com/download.html). There is also a separate package for [Krita](http://www.krita.org/) only.

We would welcome volunteers who want to build and test the Calligra Suite on OSX.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
