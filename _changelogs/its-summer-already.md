---
title: Its Summer Already!
date: 2011-04-26
author: boudewijn
categories: []
---

Yesterday, the selected projects for Google Summer of Code were announced. KDE has 51 slots this year. Out of those 51 slots, 8 are for Calligra. We're very happy to welcome so many students to our project! There is a good geographical distribution as well, with people coming from India, Brazil, Europe... Here's the low-down on who's going to do do what:

### Multithreaded Tool Actions System for Krita

Dmitry Kazakov implemented multi-threaded image projection recalculation last year. This year, he'll redesign the tool system in Krita so all actions are queued and executed in threads. A big chunk of work that will make working with Krita even smoother.

### Single Canvas Presentation mode in Stage

Slides are still the mainstay of presentations -- but there's an alternative. More and more presenters are panning and zooming over a huge canvas. Aakriti Gupta is going to make that possible in Stage. It's a bit of an experimental feature, but nothing wrong with that!

### Tagging and Management for Resources

Artists working with Karbon or Krita use many different resources, like patterns, gradients, brushes or even snippets of images. Calligra's resource handling is still stuck in the nineties. Srikanth Tiyyagura is going to improve that, to make it possible to tag resources (in a way compatible with GIMP), as well as add and remove resources. Get Hot New Stuff integration and tagging of documents using Nepomuk is also part of his project.

### Marble Map  Browser Elements for Kexi Resources

Radoslaw Wicik will extend Kexi using Marble to make it possible to show and query geographical data stored in a Kexi database.

### Annotation Support for Words

Calligra Words can load annotations -- those little comments and remarks boxes -- just fine. It cannot show them, though. Steven Kazocky will make it possible for Words to show, create and edit comments, notes and annotations in a modern way.

### Advanced Image Selection using SIOX

Bruno Morais Ferreirra is going to add a siox-based selection tool to Krita. Especially for hand-drawn images, siox is a really easy way to select foreground figures.

### Consistent Slide Manager UI in Stage

While Aakriti is going to make it possible to have presentations without slides, Paul Mendez will improve all aspects of creating presentations that do use slides. Paul has already been hacking on Stage before.

### PSD import/export for Krita

Krita is a tool every artist should be able to use -- but what if you still have a lot of images in Adobe's proprietary file format. Photoshop doesn't support the OpenRaster interchange format, so you're stuck. Until the end of the summer, that is, since Siddharth Sharma is going to work on PSD import and export support for Krita.

### In the end

There were quite a few really good proposals that didn't get selected. We're really sad about that, but there is a limited number of slots and a limited number of mentors. But you can still work on Words, Kexi, Stage, Tables, Flow, Karbon, Krita, Plan if you want to! We haven't rejected you or your ideas, you're all welcome!
